import { Database } from '../database_abstract'

import { newDb, IMemoryDb } from 'pg-mem'

export class PostgreStrategy extends Database {
    _instance: IMemoryDb

    constructor() {
        super()
        this.getInstance()
    }

    private async getInstance() {
        const db = newDb()

        db.public.many(`
            CREATE TABLE flights (
                code VARCHAR(5) PRIMARY KEY,
                origin VARCHAR(50),
                destination VARCHAR(50),
                status VARCHAR(50)
            );
        `)

        db.public.many(`
            INSERT INTO flights (code, origin, destination, status)
            VALUES ('LH123', 'Frankfurt', 'New York', 'on time'),
                     ('LH124', 'Frankfurt', 'New York', 'delayed'),
                        ('LH125', 'Frankfurt', 'New York', 'on time')
        `)
        // person
        db.public.many(`
    CREATE TABLE persons (
        email VARCHAR(50) PRIMARY KEY,
        name VARCHAR(100),
        gender VARCHAR(50)
    );
`)

        PostgreStrategy._instance = db

        return db
    }

    public async getFlights() {
        return PostgreStrategy._instance.public.many('SELECT * FROM flights')
    }

    public async addFlight(flight: {
        code: string
        origin: string
        destination: string
        status: string
    }) {
        return PostgreStrategy._instance.public.many(
            `INSERT INTO flights (code, origin, destination, status) VALUES ('${flight.code}', '${flight.origin}', '${flight.destination}', '${flight.status}')`
        )
    }
    public async updateFlightStatus(code: string) {
        const status = 'new'
        return PostgreStrategy._instance.public.many(
            `UPDATE flights SET status='${status}' WHERE code='${code}'`
        )
    }
    // persons

    public async createPerson(person: {
        email: string
        name: string
        gender: string
    }) {
        const { email, name, gender } = person
        return PostgreStrategy._instance.public.many(
            `INSERT INTO persons (email, name, gender) VALUES ('${email}', '${name}', '${gender}')`
        )
    }
    public async findPersonByEmail(email: string) {
        return PostgreStrategy._instance.public.many(
            `SELECT * FROM persons WHERE email = '${email}'`
        )
    }
}
