const assert = require('assert')
// import { PersonsModel } from '../src/models/persons.model'
// import { db } from '../src/memory-database'
// import { PostgreStrategy } from './sr'
import { after } from 'mocha'
import { PostgreStrategy } from '../src/databases/postgres/postgres'

let dbInstance: PostgreStrategy
beforeEach(async () => {
    // await db({ test: true })
    dbInstance = new PostgreStrategy()
})

describe('Persons Model', async () => {
    it('Allows to create two persons with different emails', async () => {
        // Arrange
        const person1 = {
            email: 'person1@sample.com',
            name: 'person1',
            gender: 'f',
        }
        const person2 = {
            email: 'person2@sample.com',
            name: 'person2',
            gender: 'f',
        }
        // Act
        await dbInstance.createPerson(person1)
        await dbInstance.createPerson(person2)

        const r1 = await dbInstance.findPersonByEmail(person1.email)
        const r2 = await dbInstance.findPersonByEmail(person2.email)
        // Assert
        assert.equal(person1.email, r1[0].email)
        assert.equal(person2.email, r2[0].email)
    })

    // it('Prevents creating a person that already exists on the Database', async () => {
    //     // Arrange
    //     // Act
    //     // Assert
    // })
})
